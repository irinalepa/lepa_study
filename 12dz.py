
#  Задание 1
# Установите и настройте на своем локальном компьютере Git.
# готово
# Задание 2
# Создайте папку с двумя подпапками. В каждой подпапке добавьте несколько файлов.

import os
path = os.getcwd()
print ("Текущая рабочая директория %s" % path)

path = "E:/PY_учеба/12_30.08.21 системы контроля версий/1folder"
path1 = "E:/PY_учеба/12_30.08.21 системы контроля версий/1folder/1_1folder"
path2 = "E:/PY_учеба/12_30.08.21 системы контроля версий/1folder/1_2folder"

def Path(x):
    try:
        os.mkdir(x)
    except OSError:
        print("Создать директорию %s не удалось" % x)
    else:
        print("Успешно создана директория %s " % x)

# Path(path)
# Path(path1)
# Path(path2)


#
#
file1 = "itsahardlife1.txt"
file2 = "itsahardlife2.txt"
file3 = "itsahardlife3.txt"
file4 = "itsahardlife4.txt"
file5 = "itsahardlife5.txt"
file6 = "itsahardlife6.txt"
#
def File(x, y):
        filepath = os.path.join(x, y)
        f = open(filepath, "w")
        f.close()
#
# File(path1, file1)
# File(path1, file2)
# File(path1, file3)
# File(path2, file4)
# File(path2, file5)
# File(path2, file6)


# ДАЛЕЕ Я ПРОСТО ПЕРЕКОПИРОВАЛА ИЗ ТЕРМИНАЛА СТРОКИ КАК ДЕЛАЛА ДЗ


# Задание 3
# Создайте репозиторий в главной папке.

#PS E:\PY_учеба> cd 'E:/PY_учеба/12_30.08.21 системы контроля версий/1folder'
#PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git init



# Задание 4
# Добавьте содержимое двух подпапок в индекс репозитория с помощью команды: git add.

# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git add *.txt


# Задание 5
# Создайте commit на основании подпапок, добавленных
# в индекс. Используйте команду: git commit.

# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git commit -m "содержимое 2х подпапок"
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git log
#
#
# Задание 6
# Измените содержимое одного из файлов в подпапке
# и создайте commit с новым данными.

# filepath = os.path.join(path2, file4)
# workfile = open(filepath, 'w')
# workfile.write("вношу изменения в файл")
# workfile.close()



#  cd 'E:/PY_учеба/12_30.08.21 системы контроля версий/1folder/1_2folder'
# git add itsahardlife4.txt
# git commit -m  'изменения в 4 файле'


# Задание 7
# Создайте новую подпапку, наполните её данными. После этого создайте commit c содержимым новой подпапки.

#
path11 = "E:/PY_учеба/12_30.08.21 системы контроля версий/1folder/1_3folder"
#
file11 = "itsahardlife11.txt"
file12 = "itsahardlife12.txt"
file13 = "itsahardlife13.txt"
file14 = "itsahardlife14.txt"
#
# Path(path11)
# File(path11, file11)
# File(path11, file12)
# File(path11, file13)
# File(path11, file14)

# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git add 1_3folder
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git add *.txt
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder>  git status
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git commit -m "содержимое 3й версии"



# Задание 8
# Внесите изменения в содержимое файла в новой подпапке, сохраните изменения (не создавайте новый commit).
# Отмените изменения в локальной копии файла с помощью
# команды: git checkout.

# filepath3 = os.path.join(path11, file14)
# workfile3 = open(filepath3, 'w')
# workfile3.write("вношу изменения в файл.. бла, бла, бла")
# workfile3.close()

# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder>  cd 'E:\PY_учеба\12_30.08.21 системы контроля версий\1folder\1_3folder'
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder\1_3folder> git checkout


# Задание 9
# Внесите изменения в содержимое файла в новой подпапке, добавьте изменения в индекс (не создавайте новый commit). Отмените изменения в индексе с помощью
# команды: git reset.

# filepath3 = os.path.join(path11, file14)
# workfile3 = open(filepath3, 'w')
# workfile3.write("и снова вношу изменения в файл.. бла, бла, бла")
# workfile3.close()

# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder>  git diff
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git add .
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git status
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git reset HEAD itsahardlife14.txt


# Задание 10:
# Внесите изменения в содержимое файла в новой подпапке, добавьте изменения в индекс, создайте новый commit.
# Отмените изменения в последнем commit путем создания
# нового commit с помощью команды: git revert.

# filepath3 = os.path.join(path11, file12)
# workfile3 = open(filepath3, 'w')
# workfile3.write("еще вношу изменения в файл.. бла, бла, бла")
# workfile3.close()

# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git add .
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git status
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder\1_3folder> git commit -m "содержимое 12 файла"
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder\1_3folder> git log
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git revert HEAD --no-edit
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git status


# Задание 11
# Удалите несколько последних commit. Используйте
# команду: git reset с опцией --hard.

# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git log
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git reset --hard 7248edd2b5382c5f2debaa22d7008689f6c8cda5
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git status

# Задание 12
# Внесите изменения в последний commit не создавая
# нового. Используйте: git commit с ключом --amend.

# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git commit --amend -m "содержимое 3й версии (+4файла)"
# PS E:\PY_учеба\12_30.08.21 системы контроля версий\1folder> git log

